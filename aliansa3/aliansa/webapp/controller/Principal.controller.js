sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "sap/ui/model/json/JSONModel",
        "sap/ui/model/Filter",
        'jquery.sap.global',
        'sap/m/MessageBox'
    ],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
	function (Controller,JSONModel,Filter,jQuery,MessageBox) {
		"use strict";

		return Controller.extend("ns.aliansa.controller.Principal", {
			onInit: function () {
                var oModel = new JSONModel("model/products.json");
                this.getView().setModel(oModel);
                this._tmp=[];
                this.localModel = this.getView().getModel("localModel");
        },
        _validateInput: function(oInput) {
			var oBinding = oInput.getValue("value");
            var sValueState = "None";
            var nameImput =  oInput.mProperties.name;
            var bValidationError = false;
            var noSpaces = oBinding.replace(/ /g, "");  //Remove leading spaces
            if (noSpaces.length==0 ) {
                        sValueState = "Error";
                        bValidationError = true;
            }
            oInput.setValueState(sValueState);
            return bValidationError;
        },
         onPress:function (event){

            var that = this;
            var oView = this.getView();
            var aInputs = [
                oView.byId("numeroAlmacen"),
                oView.byId("tipoAlmacen"),
                oView.byId("ubicacionOrigen")
                        ];
			var bValidationError = false;
            jQuery.each(aInputs, function (i, oInput) {
                    bValidationError = that._validateInput(oInput) || bValidationError;
                });
            
            if (!bValidationError) {
                var numeroAlmacen = oView.byId("numeroAlmacen").getValue();
                var tipoAlmacen = oView.byId("tipoAlmacen").getValue();
                var ubicacionOrigen = oView.byId("ubicacionOrigen").getValue();
                var aFilters = [];
                var oCombinedFilterG;
                var oBinding = this.byId("productList").getBinding("items");
                oCombinedFilterG = new Filter(
                                            [new Filter("nroAlmacen", "EQ", numeroAlmacen),
                                            new Filter("tipoAlmacen", "EQ", tipoAlmacen),
                                            new Filter("ubicacionOrigen", "EQ", ubicacionOrigen)
                                            ], true);
                aFilters.push(new Filter([oCombinedFilterG], false));
                this._tmp.push(new Filter([oCombinedFilterG], false));
                oBinding.filter(aFilters);
                var oPanelMenu1 = this.byId("idIconTabBar");
                oPanelMenu1.setSelectedKey("keyUnidades");
 
               } else {
               MessageBox.alert("Hubo un error en la validacion");
               return
			}

                
        },
         onListItemPress: function(event){
                var lista = event.oSource.mBindingInfos.title.binding.oContext.oModel.oData.ProductCollection;
                var indice = event.oSource.mBindingInfos.title.binding.oContext.sPath.split("/")[2];
                var elemento = lista[indice];
                this.localModel.setProperty("/detalleProducto",elemento);
                this.localModel.getProperty("/detalleProducto");
                this.localModel.refresh(true);
                var oPanelMenu1 = this.byId("idIconTabBar");
                oPanelMenu1.setSelectedKey("keyDetalle");
        },
        

         handleValueHelp : function (oEvent) {
			var sInputValue = oEvent.getSource().getValue();
            this.inputId = oEvent.getSource().getId();
            var nameImput = oEvent.oSource.mProperties.name ;
            if (!this._valueHelpDialog) {
                    this._valueHelpDialog = sap.ui.xmlfragment(
					"ns.aliansa.view.Dialog",
					this
                );
            this.getView().addDependent(this._valueHelpDialog);
            }
            this._valueHelpDialog.getBinding("items").filter([new Filter(
				"key",
				sap.ui.model.FilterOperator.Contains, sInputValue
			)]);
			this._valueHelpDialog.open(sInputValue);
        },
         handleValueHelp2 : function (oEvent) {
			var sInputValue = oEvent.getSource().getValue();
            this.inputId = oEvent.getSource().getId();
           if (!this._valueHelpDialog2) {
                    this._valueHelpDialog2 = sap.ui.xmlfragment(
					"ns.aliansa.view.TipoAlmacen",
					this
                );
            this.getView().addDependent(this._valueHelpDialog2);
            }
            this._valueHelpDialog2.getBinding("items").filter([new Filter(
				"key",
				sap.ui.model.FilterOperator.Contains, sInputValue
			)]);
			this._valueHelpDialog2.open(sInputValue);
        },
         handleValueHelp3 : function (oEvent) {
			var sInputValue = oEvent.getSource().getValue();
            this.inputId = oEvent.getSource().getId();
           if (!this._valueHelpDialog3) {
                    this._valueHelpDialog3 = sap.ui.xmlfragment(
					"ns.aliansa.view.Ubicacion",
					this
                );
            this.getView().addDependent(this._valueHelpDialog3);
            }
            this._valueHelpDialog3.getBinding("items").filter([new Filter(
				"key",
				sap.ui.model.FilterOperator.Contains, sInputValue
			)]);
			this._valueHelpDialog3.open(sInputValue);
        },

        _handleValueHelpSearch : function (evt) {
			var sValue = evt.getParameter("value");
			var oFilter = new Filter(
				"value",
				sap.ui.model.FilterOperator.Contains, sValue
			);
			evt.getSource().getBinding("items").filter([oFilter]);
		},
        _handleValueHelpClose : function (evt) {
            var oSelectedItem = evt.getParameter("selectedItem");
           if (oSelectedItem) {
				var productInput = this.byId(this.inputId);
				productInput.setValue(oSelectedItem.mProperties.description);
			}
			evt.getSource().getBinding("items").filter([]);
		}


		});
    });
